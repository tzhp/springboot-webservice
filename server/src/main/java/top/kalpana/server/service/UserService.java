package top.kalpana.server.service;


import top.kalpana.server.entity.User;

public interface UserService {

    User getById(User User);
}
