package top.kalpana.server.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.kalpana.server.service.UserService;

import javax.xml.ws.Endpoint;

@Configuration
public class WebServiceConfig {
    @Autowired
    private Bus bus;
    @Autowired
    private UserService userService;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, userService);
        // 在该服务中添加拦截器
//        endpoint.getInInterceptors().add(new CustomInterceptor());
        endpoint.publish("/user");
        return endpoint;
    }
}