package top.kalpana.springboot;

public class TestStaticWebService {
    public static void main(String[] args) {
        UserServiceImplService userServiceImplService = new UserServiceImplService();

        UserServiceImpl userServiceImplPort = userServiceImplService.getUserServiceImplPort();
        User user = new User();
        user.setId(2L);
        User byId = userServiceImplPort.getById(user);
        System.out.println("静态测试返回的数据: " + byId);
    }
}
