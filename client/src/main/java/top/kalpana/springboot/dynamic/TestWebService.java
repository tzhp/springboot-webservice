package top.kalpana.springboot.dynamic;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import top.kalpana.springboot.User;

import javax.xml.namespace.QName;

public class TestWebService {
    public static void main(String[] args) {
//JaxWsDynamicClientFactory 只要指定服务器端wsdl文件的位置，然后指定要调用的方法和方法的参数即可，不关心服务端的实现方式
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://localhost:8080/student-service/user?wsdl");
        // 设置出口拦截器
//        client.getOutInterceptors().add(new ClientLoginInterceptor("admin", "admin"));
        Object[] objects;
        try {
            //参数1 是targetNamespace的空间名，参数2 是方法名
            QName opName = new QName("http://springboot.kalpana.top", "getById");
            User user = new User();
            user.setId(3L);
            objects = client.invoke(opName, user);
            System.out.println("返回数据:" + objects[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
